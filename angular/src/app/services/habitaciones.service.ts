import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { HabitacionModel } from "../models/habitacion.model";
import { NgForm } from "@angular/forms";
import { map, delay } from "rxjs/operators";
import { Observable, BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class HabitacionesService {
  private url = "https://hotel-a1309.firebaseio.com";

  constructor(private http: HttpClient) {
    console.log("hola2");
  }
  crearhabitacion(habitacion: HabitacionModel) {
    this.http
      .post(`https://hotel-a1309.firebaseio.com/cliente.json`, habitacion)
      .subscribe((resp) => {
        console.log(resp);
      });
  }

  gethabitaciones() {
    return this.http
      .get(`${this.url}/.json`)
      .pipe(map((resp) => this.crearArreglo(resp)));
  }

  gethabitacionesById(id: string) {
    return this.http.get(`${this.url}/cliente/` + id + `/.json`);
  }

  getClientes() {
    return this.http.get(`${this.url}/cliente/.json`);
  }

  getClienteById(id: string) {
    return this.http.get(`${this.url}/cliente/` + id + `/.json`);
  }

  getComedor() {
    return this.http.get(`${this.url}/comedor/.json`);
  }

  getComedorById(id: string) {
    return this.http.get(`${this.url}/comedor/` + id + `/.json`);
  }

  private crearArreglo(habitacionesObj: object) {
    const habitaciones: HabitacionModel[] = [];
    if (habitacionesObj === null) {
      return [];
    }
    Object.keys(habitacionesObj).forEach((key) => {
      const habitacion: HabitacionModel = habitacionesObj[key];
      habitacion.id = key;
      habitaciones.push(habitacion);
    });
    return habitaciones;
  }

  actualizarHabitacion(id: string, habitacion: HabitacionModel) {
    this.crearhabitacion(habitacion);
    return this.http.delete(`${this.url}/cliente/${id}.json`).subscribe();
  }

  borrarhabitacion(id: string) {
    return this.http.delete(`${this.url}/cliente/${id}.json`).subscribe();
  }

  // onDeletePost(id: string) {
  //   return this.http
  //     .delete(`https://my-angular8-prjt.firebaseio.com/posts/${id}.json`)
  //     .subscribe();
  // }
  getProveedores(){
    return this.http.get(`${this.url}/provedor/.json`);
  }

  borrarProveedor(id: string) {
    return this.http.delete(`${this.url}/provedor/${id}.json`).subscribe();
  }
}
