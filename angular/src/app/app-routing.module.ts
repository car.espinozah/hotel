import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { RegistroComponent } from './pages/registro/registro.component';
import { LoginComponent } from './pages/login/login.component';
import { ReactiveComponent } from './pages/reactive/reactive.component';
import { HabitacionesComponent } from './pages/habitaciones/habitaciones.component';
import { HabitacionComponent } from './pages/habitacion/habitacion.component';
import { HomeComponent } from './pages/home/home.component';
import { NavarComponent } from './pages/navar/navar.component';
import { ComedorComponent } from './pages/comedor/comedor.component';
import { ProveedoresComponent } from './pages/proveedores/proveedores.component';
import { OrdencompraComponent } from './pages/ordencompra/ordencompra.component';
import { InstruccionesComponent } from './pages/instrucciones/instrucciones.component';
import { ListadeProvedoresComponent } from './pages/listade-provedores/listade-provedores.component';
import { NavarprovedoresComponent } from './pages/navarprovedores/navarprovedores.component';





const routes: Routes = [
  { path: 'registro', component: RegistroComponent },
  { path: 'login', component: LoginComponent },
  { path: 'reactivo', component: ReactiveComponent },
  { path: 'habitacion/:id', component: HabitacionComponent },
  { path: 'habitaciones', component: HabitacionesComponent },
  { path: 'home', component: HomeComponent },
  { path: 'navar', component: NavarComponent  },
  { path: 'comedor', component: ComedorComponent },
  { path: 'proveedores', component: ProveedoresComponent },
  { path: 'ordencompra', component: OrdencompraComponent },
  { path: 'instrucciones', component: InstruccionesComponent},
  { path: 'listadeprovedores', component: ListadeProvedoresComponent},
  { path: 'navarprovedores', component: NavarprovedoresComponent},

  { path: '**', pathMatch: 'full', redirectTo: 'registro' }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
