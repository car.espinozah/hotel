export class HabitacionModel {
    id: string;
    nombre: string;
    apellido: string;
    correo: string;
    habitacion: number;
    empresa: string;
    disponible: boolean;
    tipocama: string;
    preciohabitacion: number;
    precioComida: number;

    constructor() {
        this.disponible = true;
        this.precioComida = 0;
    }
}
