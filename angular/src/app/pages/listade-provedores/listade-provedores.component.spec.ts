import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListadeProvedoresComponent } from './listade-provedores.component';

describe('ListadeProvedoresComponent', () => {
  let component: ListadeProvedoresComponent;
  let fixture: ComponentFixture<ListadeProvedoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListadeProvedoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListadeProvedoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
