import { Component, OnInit } from "@angular/core";
import { HabitacionesService } from "../../services/habitaciones.service";

@Component({
  selector: "app-listade-provedores",
  templateUrl: "./listade-provedores.component.html",
  styleUrls: ["./listade-provedores.component.css"],
})
export class ListadeProvedoresComponent implements OnInit {
  constructor(private HabitacionesService: HabitacionesService) {}
  proveedores: any;


  ngOnInit() {
    this.HabitacionesService.getProveedores().subscribe((resp) => (this.proveedores = resp)
    );
  }

  //NUEVA FUNCION DE BORRAR PROVEEDOR
  borrarProve(proveedor: any) {
    var idProveedor;

    for (var key in proveedor) {
      if (String(idProveedor).length != 20) {
        idProveedor = proveedor[key];
      }
    }

    this.HabitacionesService.borrarProveedor(idProveedor);
  }
}
