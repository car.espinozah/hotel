import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { HttpClient } from '@angular/common/http';
import { ProvedorModel } from '../../models/provedor.model';
import swalWithBootstrapButtons from 'sweetalert2/dist/sweetalert2.js';


@Component({
  selector: 'app-proveedores',
  templateUrl: './proveedores.component.html',
  styleUrls: ['./proveedores.component.css']
})
export class ProveedoresComponent implements OnInit {
  provedor: ProvedorModel = new ProvedorModel();

  constructor( private autn: AuthService , private Router: Router , private http: HttpClient ) { }

  ngOnInit() {
  }

  guardar(form: NgForm) {
    if (form.invalid) {
      console.log('formulario no valido');
    }
    this.http.post(`https://hotel-a1309.firebaseio.com/provedor.json`, this.provedor).subscribe( resp => {console.log(resp); });

    swalWithBootstrapButtons.fire(
      'EXITO!',
      'Proveedor registrado.',
      'success'
    )
    var d = new Date();
    const ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(d)
    const mo = new Intl.DateTimeFormat('en', { month: 'short' }).format(d)
    const da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(d)
    var boleta = '0000'+ye+da;

    // boleta
    //   - idFirebase
    //     - numeroBoleta
    //     - fecha
    //     - monto

    alert('su b oleta es la numero ' + boleta);
  }
}
