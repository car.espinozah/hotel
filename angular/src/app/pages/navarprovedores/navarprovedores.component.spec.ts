import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavarprovedoresComponent } from './navarprovedores.component';

describe('NavarprovedoresComponent', () => {
  let component: NavarprovedoresComponent;
  let fixture: ComponentFixture<NavarprovedoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavarprovedoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavarprovedoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
