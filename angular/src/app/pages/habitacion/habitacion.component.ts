import { Component, OnInit } from "@angular/core";
import { HabitacionModel } from "../../models/habitacion.model";
import { NgForm } from "@angular/forms";
import { HabitacionesService } from "../../services/habitaciones.service";
import { Router } from "@angular/router";
import { AuthService } from "../../services/auth.service";
import { HttpClient } from "@angular/common/http";
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: "app-habitacion",
  templateUrl: "./habitacion.component.html",
  styleUrls: ["./habitacion.component.css"],
})
export class HabitacionComponent implements OnInit {
  habitacion: HabitacionModel = new HabitacionModel();
  // HabitacionesService: hola = new HabitacionesService();
  // tslint:disable-next-line: no-shadowed-variable
  constructor(
    private autn: AuthService,
    private Router: Router,
    private http: HttpClient
  ) {}

  ngOnInit() {}

  guardar(form: NgForm) {
    console.log("formulario", form);

    if (form.invalid) {
      console.log("formulario no valido");
      alert("formulario no valido");
    } 
    else {
      console.log(this.habitacion);

      this.http
        .post(
          `https://hotel-a1309.firebaseio.com/cliente.json`,
          this.habitacion
        )
        .subscribe((resp) => {
          console.log(resp);
        });
      this.Router.navigate(["/ordencompra"]);
    }

    Swal.fire({
      title: 'Espere',
      text: 'Cliente Registrado',
      type: 'info',
      allowOutsideClick: false
    });
    




  }
}
