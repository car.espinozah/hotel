import { Component, OnInit } from '@angular/core';
import { ComedorModel } from '../../models/comedor.model';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2/dist/sweetalert2.js';



@Component({
  selector: 'app-comedor',
  templateUrl: './comedor.component.html',
  styleUrls: ['./comedor.component.css']
})
export class ComedorComponent implements OnInit {
  comedor: ComedorModel = new ComedorModel();
  constructor( private autn: AuthService , private Router: Router , private http: HttpClient ) {  }

  ngOnInit() {
  }

  guardar(form: NgForm) {
    if (form.invalid) {
      console.log('formulario no valido');
    }
    console.log(this.comedor);


    this.http.post(`https://hotel-a1309.firebaseio.com/comedor.json`, this.comedor).subscribe( resp => {console.log(resp); });


    Swal.fire({
      title: 'Espere',
      text: 'Comida registrada',
      type: 'info',
      allowOutsideClick: false
    });
  }

}
