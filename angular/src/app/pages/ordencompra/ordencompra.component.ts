import { Component, OnInit } from "@angular/core";
import { HabitacionModel } from "../../models/habitacion.model";
import { NgForm } from "@angular/forms";
import { HabitacionesService } from "../../services/habitaciones.service";

import { Router } from "@angular/router";
import { AuthService } from "../../services/auth.service";
import { HttpClient } from "@angular/common/http";
import { ComedorModel } from "../../models/comedor.model";
import { ComedorComponent } from "../comedor/comedor.component";
import { FormBuilder, Validators } from "@angular/forms";

@Component({
  selector: "app-ordencompra",
  templateUrl: "./ordencompra.component.html",
  styleUrls: ["./ordencompra.component.css"],
})
export class OrdencompraComponent implements OnInit {
  habitacion: HabitacionModel = new HabitacionModel();
  habita: HabitacionModel[] = [];
  comedor: ComedorModel[] = [];
  comidas: any;
  comidaID: any;
  clientes: any;
  clienteID: any;
  comidaSeleccionada: any;
  clienteSeleccionado: any;
  addressForm = this.fb.group({
    idPersona: [null, Validators.required],
    idComida: [null, Validators.required],
  });

  constructor(
    private autn: AuthService,
    private Router: Router,
    private http: HttpClient,
    private HabitacionesService: HabitacionesService,
    private fb: FormBuilder,
    private router: Router
  ) {}

  ngOnInit() {
    this.HabitacionesService.gethabitaciones().subscribe((resp) => {
      this.habita = resp;
    });

    //TRAE TODOS LOS OBJETOS DE CLIENTES
    this.HabitacionesService.getClientes().subscribe((resp) => {
      this.clientes = resp;
    });

    //TRAE TODOS LOS OBJETOS DE COMEDOR
    this.HabitacionesService.getComedor().subscribe((resp) => {
      this.comidas = resp;
    });
  }

  actualizarOrden() {
    //TRAE TODOS LOS OBJETOS DE CLIENTES
    this.HabitacionesService.getClientes().subscribe((resp) => {
      this.clientes = resp;
    });

    //TRAE TODOS LOS OBJETOS DE COMEDOR
    this.HabitacionesService.getComedor().subscribe((resp) => {
      this.comidas = resp;
    });

    //OBTIENE EL ID DEL OBJETO DE CLIENTE
    var idCliente = Object.keys(this.clientes)[
      this.addressForm.controls["idPersona"].value
    ];

    this.HabitacionesService.getClienteById(idCliente).subscribe((resp) => {
      this.clienteSeleccionado = resp;
    });

    this.habitacion = this.clienteSeleccionado;
    this.habitacion.precioComida = Number(
      this.addressForm.controls["idComida"].value
    );

    this.HabitacionesService.actualizarHabitacion(idCliente, this.habitacion);
  }
}
