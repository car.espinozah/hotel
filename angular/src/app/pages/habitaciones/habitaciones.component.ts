import { Component, OnInit } from "@angular/core";
import { HabitacionModel } from "../../models/habitacion.model";
import { HabitacionesService } from "../../services/habitaciones.service";
import { Router } from "@angular/router";
import { AuthService } from "../../services/auth.service";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: "app-habitaciones",
  templateUrl: "./habitaciones.component.html",
  styleUrls: ["./habitaciones.component.css"],
})
export class HabitacionesComponent implements OnInit {
  habita: HabitacionModel[] = [];
  cargando = false;

  // tslint:disable-next-line: no-shadowed-variable
  constructor(
    private autn: AuthService,
    private Router: Router,
    private http: HttpClient,
    private HabitacionesService: HabitacionesService
  ) {}

  ngOnInit() {
    this.cargando = true;
    // this.http.get(`https://hotel-a1309.firebaseio.com/cliente.json`, this.HabitacionesService ).gethabitaciones().subcribe( resp => {console.log(resp); });
    // [0] primer elemento del arrey
    this.HabitacionesService.gethabitaciones().subscribe((resp) => {
      console.log(resp);
      this.habita = resp;
      console.log(this.habita[0], (this.cargando = false));
    });
  }

  borrarhabita(habitacion: HabitacionModel) {
    var idCliente;

    for (var key in habitacion) {
      if (String(idCliente).length != 20){
        idCliente = habitacion[key];
      }
    }

    this.HabitacionesService.borrarhabitacion(idCliente);
    this.Router.navigate(["/habitaciones"]);
  }
}
