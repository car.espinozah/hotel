import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { RegistroComponent } from './pages/registro/registro.component';
import { LoginComponent } from './pages/login/login.component';
import { from } from 'rxjs';
import { ReactiveComponent } from './pages/reactive/reactive.component';
import { HabitacionesComponent } from './pages/habitaciones/habitaciones.component';
import { HabitacionComponent } from './pages/habitacion/habitacion.component';
import { NavarComponent } from './pages/navar/navar.component';
import { HomeComponent } from './pages/home/home.component';
import { ComedorComponent } from './pages/comedor/comedor.component';
import { ProveedoresComponent } from './pages/proveedores/proveedores.component';
import { OrdencompraComponent } from './pages/ordencompra/ordencompra.component';
import { BuscarClienteComponent } from './pages/buscar-cliente/buscar-cliente.component';
import { InstruccionesComponent } from './pages/instrucciones/instrucciones.component';
import { NavarprovedoresComponent } from './pages/navarprovedores/navarprovedores.component';
import { ListadeProvedoresComponent } from './pages/listade-provedores/listade-provedores.component';





@NgModule({
  declarations: [
    AppComponent,
    RegistroComponent,
    LoginComponent,
    ReactiveComponent,
    HabitacionesComponent,
    HabitacionComponent,
    NavarComponent,
    HomeComponent,
    ComedorComponent,
    ProveedoresComponent,
    OrdencompraComponent,
    BuscarClienteComponent,
    InstruccionesComponent,
    NavarprovedoresComponent,
    ListadeProvedoresComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
